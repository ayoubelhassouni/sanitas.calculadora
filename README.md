# Calculadora Spring-Boot
Calculadora como micro-servicio (API) utilizando Spring-boot y Maven con funcionalidades básicas de sumar y restar.

La comunicación se realiza mediante API REST, concretamente desde un método POST que se explicará como acceder a él más abajo.

## Requisitos
Tener una version actualizada de MAVEN y otra de JAVA en nuestro caso usamos la 3.8 de MAVEN y la 17 de JAVA.

La version de JAVA puede cambiarse a través del fichero pom.xml.

## Guía de Uso

1. Clonar el repositorio de GitLab.
2. Ejecutar una verificación de Maven.
3. Instalación limpia de Maven.
4. Ejecutar el JAR de la carpeta generada /target. 

Abrir una consola y ejecutar:

```
git clone https://gitlab.com/ayoubelhassouni/sanitas.calculadora.git
cd calculadora
mvn validate
mvn verify
mvn clean install
```
Debería haberse generado una carpeta `/target` con el JAR definitivo del programa `calculadora-0.0.1.jar`. 

Para ejecutarlo usamos el siguiente comando:

`java -jar target/calculadora-0.0.1.jar`

Tambien podemos levantar la aplicación a través de un IDE como Eclipse o InteliJIdea ejecutando la siguiente clase:

`/src/java/CalculadoraAplication.java`

Si todo ha ido correcto debería estar ejecutando la aplicación en la siguiente URL: `http://localhost:8080/api/calculate`

## API REST - Método POST

He implementado un método de tipo POST debido a que en el futuro habrá N operaciones y se necesita escalabilidad y un método de tipo GEt tiene una limitación de caracteres en la URL,
un POSt debría solventar este problema.

Para interactuar con el método a se requiere un body parecido al que dejo a continuación:

`{
"operator1": "12.0",
"operator2": "4.0",
"operation": "SUM"
}`

Comando cURL para ejecutar la peticion de prueba:

`
curl --location --request POST 'http://localhost:8080/api/calculate' \
--header 'Content-Type: application/json' \
--data-raw '{
"operator1": "98.0",
"operator2": "4.0",
"operation": "SUM"
'
`

Podemos  cambiar los valores de los operators teniendo en cuenta que no pueden ser valores enteros, solo decimales, y además también podemos cambiar la operación teniendo en cuenta que solo acepta las que tengamos implementadas, en nuestro caso suma y resta.

Procedemos a introducir la URL superior y el body en un API-Tester, por ejemplo POSTMAN.

Y hacemos la request, nos devolverá un resultado con el valor numérico con decimales.

`8.0`

## Ejecutar los tests

Si has instalado todas las dependencias de MAVEN en local, para ejecutar los test debes ejecutar el comando `mvn clean test`.
