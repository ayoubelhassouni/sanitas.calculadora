package com.sanitas.calculadora.controller;

import com.sanitas.calculadora.model.OperationDTO;
import com.sanitas.calculadora.service.CalculadoraService;
import io.corp.calculator.TracerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Gestiona el método POST /api/calculate
 */
@RestController
@RequestMapping("/api")
public class ControllerCalculadora {

    @Autowired
    private CalculadoraService calculadoraService;
    @Autowired
    private TracerImpl tracer;


    @PostMapping(value = "/calculate")
    public ResponseEntity<Double> calculate(@RequestBody OperationDTO operationDTO) {

        Double result = this.calculadoraService.calculate(operationDTO);

        tracer.trace(result);

        return ResponseEntity.ok(result);
    }

}
