package com.sanitas.calculadora.exception;

public class CalculateException extends RuntimeException{

    public CalculateException(String errorMessage) {
        super(errorMessage);
    }

}
