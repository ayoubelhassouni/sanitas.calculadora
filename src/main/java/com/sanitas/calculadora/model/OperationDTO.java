package com.sanitas.calculadora.model;

import com.sanitas.calculadora.utility.Operation;


import java.math.BigDecimal;


    public record OperationDTO(BigDecimal operator1, BigDecimal operator2, Operation operation) {
    }

