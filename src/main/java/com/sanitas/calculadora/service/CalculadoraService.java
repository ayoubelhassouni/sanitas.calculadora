package com.sanitas.calculadora.service;

import com.sanitas.calculadora.model.OperationDTO;

public interface CalculadoraService {

    /**
     *
     * Este metodo se utiliza para realizar operaciones aritmeticas en base a los parametros introducidos como argumentos
     *
     *
     *
     *
     * @return el resultado tipo double de la operacion.
     *
     */


    Double calculate(OperationDTO operationDTO);
}
