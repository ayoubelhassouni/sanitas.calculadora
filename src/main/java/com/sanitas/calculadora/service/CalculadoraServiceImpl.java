package com.sanitas.calculadora.service;

import com.sanitas.calculadora.exception.CalculateException;
import com.sanitas.calculadora.model.OperationDTO;
import org.springframework.stereotype.Service;

/**
 * Implementación del servicio de la interfaz {@link CalculadoraService}
 */
@Service
public class CalculadoraServiceImpl implements CalculadoraService {


    @Override
    public Double calculate(OperationDTO operationDTO) {

        if (operationDTO == null) {
            throw new CalculateException("Your operation could not be processed ");
        }

        return switch (operationDTO.operation()) {
            case SUM -> operationDTO.operator1().add(operationDTO.operator2()).doubleValue();
            case SUBTRACT -> operationDTO.operator1().subtract(operationDTO.operator2()).doubleValue();
        };
    }
}
