package com.sanitas.calculadora.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanitas.calculadora.model.OperationDTO;
import com.sanitas.calculadora.service.CalculadoraService;
import com.sanitas.calculadora.utility.Operation;
import io.corp.calculator.TracerImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static com.sanitas.calculadora.utility.Operation.SUM;
import static java.math.BigDecimal.*;
import static java.math.BigDecimal.ONE;
import static java.math.BigInteger.TEN;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(ControllerCalculadora.class)
class CalculadoraApplicationControllerTest {

    @MockBean
    CalculadoraService calculadoraService;

    @MockBean
    TracerImpl tracer;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testRestController() throws Exception {


        OperationDTO operationDTO = new OperationDTO(BigDecimal.ONE, BigDecimal.TEN, SUM);

        Mockito.when(calculadoraService.calculate(operationDTO)).thenReturn(11.0);
        doNothing().when(tracer).trace(any());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/calculate")
                        .content(objectMapper.writeValueAsString(operationDTO))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());


    }


}
