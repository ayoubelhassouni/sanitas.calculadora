package com.sanitas.calculadora.service;

import java.math.BigDecimal;

import com.sanitas.calculadora.exception.CalculateException;
import com.sanitas.calculadora.model.OperationDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.sanitas.calculadora.utility.Operation.SUBTRACT;
import static com.sanitas.calculadora.utility.Operation.SUM;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class CalculadoraServiceImplTest {

    @InjectMocks
    CalculadoraServiceImpl calculadoraService;

    @Test
    public void testSuma()
    {

        OperationDTO operationDTO = new OperationDTO(BigDecimal.ONE, BigDecimal.TEN, SUM );
        assertEquals(calculadoraService.calculate(operationDTO), 11.0);
    }

    @Test
    public void testResta()
    {

        OperationDTO operationDTO = new OperationDTO(BigDecimal.TEN, BigDecimal.TEN, SUBTRACT );
        assertEquals(calculadoraService.calculate(operationDTO), 0.0);

    }

    @Test
    @DisplayName("Calculate Should Throw Exception using operationDTO null")
    public void calculateShouldThrowException()
    {

        assertThrows(CalculateException.class, () -> {
            calculadoraService.calculate(null);
        });
    }
}
